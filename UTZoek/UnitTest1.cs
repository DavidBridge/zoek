using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zoek.Models;
using System.Collections.Generic;

namespace UTZoek
{
    [TestClass]
    public class UnitTest1
    {
        // Check operation of DependencyModel
        [TestMethod]
        public void Test_DependencyModel_Splits_Valid_string()
        {
            var result = new DependencyModel("A=>B");
            Assert.IsInstanceOfType(result, typeof(DependencyModel));
            Assert.AreEqual("a", result.Child);
            Assert.AreEqual("b", result.Parent);
        }

        // Check operation of DependencyModel
        [TestMethod]
        public void Test_DependencyModel_Does_not_Split_Invalid_string()
        {
            var result = new DependencyModel("test");
            Assert.IsInstanceOfType(result, typeof(DependencyModel));
            Assert.IsNull(result.Child);
            Assert.IsNull(result.Parent);
        }


        // Below here we test different scenarios

        [TestMethod]
        public void TestMethod_NoDependencies_NoTasks()
        {
            List<string> dependencies = new List<string>() { };
            var d = new Zoek.Services.Dependencies(dependencies);

            string tasks = "";

            var sortedTasks = d.SortTasksByDependency(tasks);

            Assert.AreEqual("", sortedTasks);
        }

        [TestMethod]
        public void TestMethod_NoDependencies_TasksInOriginalOrder()
        {
            List<string> dependencies = new List<string>() { };
            var d = new Zoek.Services.Dependencies(dependencies);

            string tasks = "a,b";

            var sortedTasks = d.SortTasksByDependency(tasks);

            Assert.AreEqual("a,b", sortedTasks);
        }

        [TestMethod]
        public void TestMethod_AB()
        {
            List<string> dependencies = new List<string>() {
                "A=>B",
            };
            var d = new Zoek.Services.Dependencies(dependencies);

            string tasks = "a,b";

            var sortedTasks = d.SortTasksByDependency(tasks);

            Assert.AreEqual("b,a", sortedTasks);
        }

        [TestMethod]
        public void TestMethod_ABCD()
        {
            List<string> dependencies = new List<string>() {
                "A=>B",
                "C=>D"
            };
            var d = new Zoek.Services.Dependencies(dependencies);

            string tasks = "a,b,c,d";

            var sortedTasks = d.SortTasksByDependency(tasks);

            Assert.AreEqual("b,a,d,c", sortedTasks);
        }

        [TestMethod]
        public void TestMethod_ABC()
        {
            List<string> dependencies = new List<string>() {
                "A=>B",
                "B=>C"
            };
            var d = new Zoek.Services.Dependencies(dependencies);

            string tasks = "a,b,c";

            var sortedTasks = d.SortTasksByDependency(tasks);

            Assert.AreEqual("c,b,a", sortedTasks);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException), "Circular reference")]
        public void TestMethod_CircularReference()
        {
            List<string> dependencies = new List<string>() {
                "A=>B",
                "B=>C",
                "C=>A"
            };
            var d = new Zoek.Services.Dependencies(dependencies);

            string tasks = "a,b,c,d";
            var sortedTasks = d.SortTasksByDependency(tasks);
        }


    }
}
