﻿using System;

namespace Zoek.Models
{
    public class DependencyModel
    {
        public DependencyModel(string dependency)
        {
            string[] separator = new string[] { "=>" };
            var dep = dependency.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            if (dep != null && dep.GetUpperBound(0) == 1)
            {
                Child = dep[0];
                Parent = dep[1];
            }
        }

        public DependencyModel(string child, string parent)
        {
            Child = child;
            Parent = parent;
        }

        private string child;
        public string Child {
            get { return child; }
            set { child = value.ToLower(); }
        }
        private string parent;
        public string Parent
        {
            get { return parent; }
            set { parent = value.ToLower(); }
        }
    }
}
