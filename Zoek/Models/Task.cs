﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoek.Models
{
    public class Task 
    {
        public Task(string taskName, int taskOrder)
        {
            TaskName = taskName;
            TaskOrder = taskOrder;
        }
        public string TaskName { get; set; }
        public int TaskOrder { get; set; }

    }
}
