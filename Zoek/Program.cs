﻿using System;
using System.Collections.Generic;

namespace Zoek
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> dependencies = new List<string>() {
                "A=>B",
                "B=>C",
                "C=>D"
            };
            var d = new Services.Dependencies(dependencies);

            // Array of task names
            string tasks = "a,b,c,d";

            var sortedTasks = d.SortTasksByDependency(tasks);

            // Show the tasks in the correct order
            Console.WriteLine(sortedTasks);
            
            Console.Out.Flush();
            var name = Console.ReadLine();

        }



    }
}