﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zoek.Models;

namespace Zoek.Services
{
    public class Dependencies
    {
        private IEnumerable<DependencyModel> _dependencyList;

        public Dependencies(List<string> dependencies)
        {
            // Convert strings to Dependency list
            _dependencyList = dependencies.Select(x => new DependencyModel(x));
            ValidateDependencyList();
        }

        /// <summary>
        /// This is a curcular reference check
        /// </summary>
        private void ValidateDependencyList()
        {
            // For each dependency in the list traverse it and see if the parent comes up in a child. if it does its an issue.
            foreach (var d in _dependencyList) {
                TraverseDependency(d, d.Parent);
            }
        }

        private void TraverseDependency(DependencyModel dependency, string start)
        {
            foreach (var d in _dependencyList.Where(x => x.Parent == dependency.Child))
            {
                if (d.Child == start) {
                    throw new FormatException("Circular reference");
                }
                TraverseDependency(d, start);
            }
        }

        /// <summary>
        /// Entry point: Call this from program
        /// Note that I could have used a simple string with Substring() but this would, in my opinion, have been too much manipulation of the imutable type
        /// This could be mitigated with use of a StringBuilder but this too has high overhead. I don't think the proposed solution is the best but it is a 100% correct solution and it works
        /// The specification did not ask for the best solution, just an accurate one and comparison of different techniques would have taken too long. I've had a busy week and this is just one task in it ;-).
        /// Unit (more like integration in this case) tests prove that the solution fits the requirement.
        /// </summary>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public string SortTasksByDependency(string tasks)
        {
            string[] taskArray = tasks.ToLower().Split(',');

            bool updatesMade = false;
            do {
                updatesMade = false;
                taskArray = SortTasks(taskArray, 0, ref updatesMade);
            }
            while (updatesMade == true);

            return string.Join(",",taskArray);
        }


        private string[] SortTasks(string[] tasks, int currentIndex, ref bool updatesMade)
        {
            if (currentIndex == tasks.GetUpperBound(0)) {
                return tasks;
            }

            if (HasDependency(currentIndex, tasks)) {
                // Move item right and check again
                MoveItemRight(currentIndex, tasks);
                updatesMade = true;
            }
            return SortTasks(tasks, currentIndex + 1, ref updatesMade);
        }

        private bool HasDependency(int index, string[] tasks)
        {
            var currentTaskName = tasks[index];
            // What are the elements to the right
            var rightTasks = tasks.Skip(index).Select(x=>x);
            // What are the dependencies of this element
            var dependencies = _dependencyList.Where(x => x.Child == currentTaskName).Select(x=>x.Parent);
            // Do they match - i.e. is there a parent to the right?
            var exists = rightTasks.Intersect(dependencies);

            return (exists.Count() >0);
        }

        private string[] MoveItemRight(int index, string[] tasks)
        {
            // Swap values
            var temp = tasks[index];
            tasks[index] = tasks[index + 1];
            tasks[index + 1] = temp;
            return tasks;
        }

    }
}
